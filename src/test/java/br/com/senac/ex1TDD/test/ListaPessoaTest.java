
package br.com.senac.ex1TDD.test;

import br.com.senac.ex1TDD.ListaPessoa;
import br.com.senac.ex1TDD.modelo.Pessoa;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class ListaPessoaTest {
    
    public ListaPessoaTest() {
    }
    
    @Test
    public void DiegoDeveSeroMaisNovo(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa diego = new Pessoa("Diego", 19);
        Pessoa alex = new Pessoa("Alex", 23);
        Pessoa samuel = new Pessoa("Samuel", 42);
        lista.add(diego);
        lista.add(alex);
        lista.add(samuel);
        
        Pessoa pessoaMaisNova = ListaPessoa.getPessoaMaisNova(lista);
        
        assertEquals(diego, pessoaMaisNova);
                
    }
    
    
    @Test
    public void samuelDeveSeroMaisNovo(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa diego = new Pessoa("Diego", 19);
        Pessoa alex = new Pessoa("Alex", 23);
        Pessoa samuel = new Pessoa("Samuel", 5);
        lista.add(diego);
        lista.add(alex);
        lista.add(samuel);
        
        Pessoa pessoaMaisNova = ListaPessoa.getPessoaMaisNova(lista);
        
        assertEquals(samuel, pessoaMaisNova);
                
    }
    
     @Test
    public void DiegoNaoDeveSeroMaisNovo(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa diego = new Pessoa("Diego", 45);
        Pessoa alex = new Pessoa("Alex", 23);
        Pessoa samuel = new Pessoa("Samuel", 42);
        lista.add(diego);
        lista.add(alex);
        lista.add(samuel);
        
        Pessoa pessoaMaisNova = ListaPessoa.getPessoaMaisNova(lista);
        
        assertNotEquals(diego, pessoaMaisNova);
                
    }
    
    
    @Test
    public void AlexNaoDeveSeroMaisNovo(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa diego = new Pessoa("Diego", 19);
        Pessoa alex = new Pessoa("Alex", 25);
        Pessoa samuel = new Pessoa("Samuel", 20);
        lista.add(diego);
        lista.add(alex);
        lista.add(samuel);
        
        Pessoa pessoaMaisNova = ListaPessoa.getPessoaMaisNova(lista);
        
        assertNotEquals(alex, pessoaMaisNova);
        
    }
    
    
    
    
    @Test
    public void SamuelDeveSerMaisVelho(){
         List<Pessoa> lista = new ArrayList<>();
        Pessoa diego = new Pessoa("Diego", 19);
        Pessoa alex = new Pessoa("Alex", 25);
        Pessoa samuel = new Pessoa("Samuel", 35);
        lista.add(diego);
        lista.add(alex);
        lista.add(samuel);
        
        Pessoa pessoaMaisVelha = ListaPessoa.getPessoaMaisVelha(lista) ; 
        
        assertEquals(samuel,pessoaMaisVelha);
        
        
    }
    
     @Test
    public void AlexNaoDeveSerMaisVelho(){
         List<Pessoa> lista = new ArrayList<>();
        Pessoa joao = new Pessoa("João", 100);
        Pessoa alex = new Pessoa("Alex", 15);
        Pessoa samuel = new Pessoa("Samuel", 20);
        lista.add(joao);
        lista.add(alex);
        lista.add(samuel);
        
        Pessoa pessoaMaisVelha = ListaPessoa.getPessoaMaisVelha(lista) ; 
        
        assertNotEquals(alex,pessoaMaisVelha);
        
        
    }
    
    
    
    
    
}
